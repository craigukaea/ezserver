from EzOpcuaServer import EzOpcuaServer
    
if __name__== "__main__":
    server = EzOpcuaServer()
    server.setEndpoint("opc.tcp://0.0.0.0:4843/freeopcua/server/")
    server.registerNamespace("http://examples.freeopcua.github.io")
    server.addObject("Foo/Bar/Baz")
    sig = server.addMethod("Foo/Bar/SayHello")
    
    server.setAvailability("Foo/Bar/SayHello", False)
    server.setUserAvailability("Foo/Bar/SayHello", False)

    server.start()
    
    sig.connect(lambda **kwargs: print("Hello World"))
    
    while True:
        pass